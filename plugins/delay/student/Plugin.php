<?php namespace Delay\Student;

use Backend;
use System\Classes\PluginBase;

/**
 * Student Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'Student',
            'description' => 'No description provided yet...',
            'author'      => 'Delay',
            'icon'        => 'icon-leaf'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {

    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
//        return []; // Remove this line to activate

        return [
            'Delay\Student\Components\MyComponent' => 'myComponent',
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
//        return []; // Remove this line to activate

        return [
            'delay.student.some_permission' => [
                'tab' => 'Student',
                'label' => 'Some permission'
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {
//        return []; // Remove this line to activate

        return [
            'student' => [
                'label'       => 'Student',
                'url'         => Backend::url('delay/student/mycontroller'),
                'icon'        => 'icon-leaf',
                'permissions' => ['delay.student.*'],
                'order'       => 500,
            ],
        ];
    }
}
