<?php namespace Delay\Student\Components;

use Cms\Classes\ComponentBase;

class Delay extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'Delay Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }
}
