<?php namespace Delay\Student\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateDelaysTable extends Migration
{
    public function up()
    {
        Schema::create('delay_student_delays', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->timestamps();
            $table->char("Delay");
        });
    }

    public function down()
    {
        Schema::dropIfExists('delay_student_delays');
    }
}
