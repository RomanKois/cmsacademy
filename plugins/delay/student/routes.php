<?php
use Studentracker\Tracker\Models\Arrival;
use Delay\Student\Models\Delay;

Route::get('/api/{id}/arrive', function () {
    return now();
});

Route::post('/api/arrive', function (){
    $time = Arrival::create(['arrival' => now()]);
    return $time;
});

Route::get('api/arrivals', function (){

    return Arrival::get();
});
