<?php namespace Studentracker\Tracker\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateArrivalsTable extends Migration
{
    public function up()
    {
        Schema::create('studentracker_tracker_arrivals', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->timestamps();
            $table->timestamp('arrival');
        });
    }

    public function down()
    {
        Schema::dropIfExists('studentracker_tracker_arrivals');
    }
}
