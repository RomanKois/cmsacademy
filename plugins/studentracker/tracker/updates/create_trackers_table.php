<?php namespace StudenTracker\Tracker\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateTrackersTable extends Migration
{
    public function up()
    {
        Schema::create('studentracker_tracker_trackers', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('studentracker_tracker_trackers');
    }
}
