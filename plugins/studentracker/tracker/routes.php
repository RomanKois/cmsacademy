<?php
    use Studentracker\Tracker\Models\Arrival;

    Route::get('/api/{id}/arrive', function () {
        return now();
    });

    Route::post('/api/arrive', function (){
       $time = Arrival::create(['arrival' => now()]);
       return $time;
    });

    Route::get('api/arrivals', function (){

        return Arrival::get();
    });
